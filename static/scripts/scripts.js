"use strict";
angular.module("ngApp").controller("AbstractNavigationController", ["$namespace", "$scope", "toggleProvider", function(a, b, c) {
    b.switches = {
        play: c.init(a + ":play", !0, !1),
        next: c.init(a + ":next", !0, !1),
        prev: c.init(a + ":prev", !0, !1),
        showAll: c.init(a + ":showAll", !0, !1),
        reset: c.init(a + ":reset", !0, !1)
    }, b.visibility = {
        play: c.init(a + ":visibility:play", !1, !0),
        next: c.init(a + ":visibility:next", !1, !0),
        prev: c.init(a + ":visibility:prev", !1, !0),
        showAll: c.init(a + ":visibility:showAll", !1, !0),
        reset: c.init(a + ":visibility:reset", !1, !0)
    }, b.play = function() {
        b.switches.play = c["switch"](a + ":play"), b.$emit(a + ":play", {
            toggle: b.switches.play,
            visibility: b.visibility.play
        })
    }, b.next = function() {
        b.switches.next = c["switch"](a + ":next"), b.$emit(a + ":next", {
            toggle: b.switches.next,
            visibility: b.visibility.next
        })
    }, b.prev = function() {
        b.switches.prev = c["switch"](a + ":prev"), b.$emit(a + ":prev", {
            toggle: b.switches.prev,
            visibility: b.visibility.prev
        })
    }, b.showAll = function() {
        b.switches.showAll = c["switch"](a + ":showAll"), b.$emit(a + ":showAll", {
            toggle: b.switches.showAll,
            visibility: b.visibility.showAll
        })
    }, b.reset = function() {
        b.switches.reset = c["switch"](a + ":reset"), b.$emit(a + ":reset", {
            toggle: b.switches.reset,
            visibility: b.visibility.reset
        })
    }
}]), angular.module("ngApp").provider("routeProvider", function() {
    this.$get = ["$route", "$location", function(a, b) {
        var c = function() {
            this.routes = [], this.activeIndex = !1
        };
        return c.prototype.init = function(b) {
            if (this.routes.length > 0) return !1;
            for (var c in a.routes) void 0 !== a.routes[c].caseInsensitiveMatch && "null" !== c && a.routes[c].controllerAs === b && this.routes.push(c);
            this.activeIndex || (this.activeIndex = this.routes.indexOf(a.current.$$route.originalPath))
        }, c.prototype.next = function() {
            ++this.activeIndex > this.routes.length - 1 && (this.activeIndex = this.routes.length - 1), b.path(this.routes[this.activeIndex])
        }, c.prototype.prev = function() {
            --this.activeIndex < 0 && (this.activeIndex = 0), b.path(this.routes[this.activeIndex])
        }, c.prototype.last = function() {
            this.activeIndex = this.routes.length - 1, b.path(this.routes[this.activeIndex])
        }, c.prototype.first = function() {
            this.activeIndex = 0, b.path(this.routes[this.activeIndex])
        }, c.prototype.isEnd = function() {
            return this.activeIndex === this.routes.length - 1 ? !0 : !1
        }, c.prototype.isFirst = function() {
            return 0 === this.activeIndex ? !0 : !1
        }, new c
    }]
}), angular.module("ngApp").provider("toggleProvider", function() {
    this.$get = [function() {
        var a = function() {
            this.collection = []
        };
        return a.prototype.init = function(a, b, c) {
            return void 0 == this.collection[a] && (this.collection[a] = {
                value: c,
                on: b,
                off: c
            }), this.collection[a].value
        }, a.prototype.get = function(a) {
            return void 0 !== this.collection[a] ? this.collection[a].value : void 0
        }, a.prototype.set = function(a, b) {
            return this.collection[a].value = b, this.collection[a].value
        }, a.prototype["switch"] = function(a) {
            var b = this.collection[a];
            return this.set(a, b.value === b.on ? b.off : b.on)
        }, new a
    }]
}), angular.module("ngApp").component("navigationComponent", {
    templateUrl: "views/navigationComponentView.html",
    controller: ["$controller", "$scope", "toggleProvider", function(a, b, c) {
        angular.extend(this, a("AbstractNavigationController", {
            $scope: b,
            toggleProvider: c,
            $namespace: "navigationComponent"
        })), b.$emit("navigationComponent:init"), b.$watch(function() {
            return c.get("navigationComponent:play")
        }, function(a) {
            b.switches.play = a
        }), b.$watch(function() {
            return c.get("navigationComponent:visibility:play")
        }, function(a) {
            b.visibility.play = a
        }), b.$watch(function() {
            return c.get("navigationComponent:visibility:next")
        }, function(a) {
            b.visibility.next = a
        }), b.$watch(function() {
            return c.get("navigationComponent:visibility:prev")
        }, function(a) {
            b.visibility.prev = a
        }), b.$watch(function() {
            return c.get("navigationComponent:visibility:showAll")
        }, function(a) {
            b.visibility.showAll = a
        }), b.$watch(function() {
            return c.get("navigationComponent:visibility:reset")
        }, function(a) {
            b.visibility.reset = a
        })
    }],
    bindings: {
        controllerAs: "@"
    }
}), angular.module("ngApp").component("audioComponent", {
    templateUrl: "views/audioComponentView.html",
    controller: ["$scope", "$document", "toggleProvider", "$location", function(a, b, c, d) {
        for (var e = {}, f = this.src.match(/([A-Z]+)\=?([A-Z0-9\/\.\-\_]+)/gi), g = 0; g < f.length; g++) {
            var h = f[g].match(/([a-z]+)\=([A-Z0-9\/\.\-\_]+)/i);
            e[h[1]] = h[2]
        }
        var i = ["en", "es"],
            j = ["off", "on"];
        a.lang = c.init("audioCompoment:lang", !0, !1), a.langText = i[a.lang ? 1 : 0], a.file = this.src, a.player = new Audio(e[a.langText]), a.audio = c.init("audioCompoment:autoplay", !0, !1), a.audioText = j[a.audio ? 1 : 0], a.player.muted = !a.audio, a.audioToggle = function(b) {
            a.audio = c["switch"]("audioCompoment:autoplay"), a.player.muted = !a.audio, a.$emit("audioCompoment:autoplay", a.audio), a.audioText = j[a.audio ? 1 : 0]
        }, a.langToggle = function(b) {
            a.lang = c["switch"]("audioCompoment:lang"), a.langText = b[a.lang ? 1 : 0]
        }, a.player.addEventListener("ended", function(b) {
            a.$emit("audioCompoment:audio:ended", {
                e: b
            })
        }), a.$on("$destroy", function() {
            a.player.src = ""
        }), c.init("audioCompoment:audio:play", !0, !1), a.$watch(function() {
            return c.get("audioCompoment:audio:play")
        }, function(b) {
            void 0 !== a.file && (b === !0 ? a.player.play() : a.player.pause())
        })
    }],
    bindings: {
        src: "@"
    }
}), angular.module("ngApp").component("captionsComponent", {
    templateUrl: "views/captionsComponentView.html",
    controller: ["$scope", "toggleProvider", function(a, b) {
        a.captions = b.init("captions", !0, !1), a.captionsToggle = function() {
            a.captions = b["switch"]("captions")
        }
    }]
}), angular.module("ngApp").component("popupComponent", {
    templateUrl: "views/popupComponentView.html",
    restrict: "E",
    transclude: !0,
    bindings: {
        left: "@",
        top: "@"
    },
    controller: ["$scope", function(a) {
        a.left = this.left, a.top = this.top, a.show = !1, a.toggle = function() {
            a.show = !a.show
        }
    }]
}), angular.module("ngApp").directive("bilCp", function() {
    function a(a, b, c) {
        var d = new fabric.Canvas("cp");
        d.setHeight(b), d.setWidth(a);
        var e = {
                stroke: "#c4c4c4",
                strokeWidth: 1,
                selectable: !1,
                hasControls: !1,
                hasBorders: !1,
                lockMovementX: !1,
                lockMovementY: !1
            },
            f = {
                fontSize: 14,
                fontWeight: "600",
                selectable: !1,
                fontFamily: "Source Sans Pro, sans-serif"
            },
            g = Math.floor(d.width / 2),
            h = Math.floor(d.height / 2),
            i = new fabric.Text("0", f);
        i.left = g - 10 - i.width / 2, i.top = h + 5, d.add(i);
        var j = 0,
            k = 0;
        do {
            if (k += c, j += c, k < d.height / 2 && (d.add(new fabric.Line([0, h + k, d.width, h + k], e)), d.add(new fabric.Line([0, h - k, d.width, h - k], e)), k > 0)) {
                var l = (k / c).toString(),
                    i = new fabric.Text("-" + l, f);
                i.top = h + k - i.height / 2, i.left = g - 18, d.add(i);
                var i = new fabric.Text(l, f);
                i.top = h - k - i.height / 2, i.left = g - 13, d.add(i)
            }
            if (j < d.width / 2 && (d.add(new fabric.Line([g - j, 0, g - j, d.height], e)), d.add(new fabric.Line([g + j, 0, g + j, d.height], e)), j > 0)) {
                var l = (j / c).toString(),
                    i = new fabric.Text(l, f);
                i.left = g + j - i.width / 2, i.top = h + 5, d.add(i);
                var i = new fabric.Text("-" + l, f);
                i.left = g - j - i.width + 4, i.top = h + 5, d.add(i)
            }
        } while (j < d.width || k < d.height);
        return d.add(new fabric.Line([g, 0, g, d.height], {
            stroke: "#000",
            strokeWidth: 2,
            selectable: !1,
            hasControls: !1,
            hasBorders: !1,
            lockMovementX: !1,
            lockMovementY: !1
        })), d.add(new fabric.Line([0, h, d.width, h], {
            stroke: "#000",
            strokeWidth: 2,
            selectable: !1,
            hasControls: !1,
            hasBorders: !1,
            lockMovementX: !1,
            lockMovementY: !1
        })), d
    }

    function b() {
        return {
            prepare: function() {},
            begin: function() {},
            process: function() {},
            end: function() {}
        }
    }
    return {
        transclude: !0,
        link: function(c, d, e) {
            var f = new a(600, 600, 30);
            c.originalState = f.toJSON(), c.toolbar = {
                action: new b
            }, c.isActiveTab = function(a) {
                return a === c.toolbar.action
            }, c.onClickTab = function(a) {
                c.toolbar.action = a
            }, c.action = new b, c.elements = [], c.mouse = new b, c.mouse.prepare = function() {
                f.selection = !0;
                for (var a = 0; a < f._objects.length; a++) "point" == f._objects[a].type && (f._objects[a].hasBorders = !0, f._objects[a].lockMovementX = !1, f._objects[a].lockMovementY = !1, f._objects[a].selectable = !0);
                f.renderAll()
            }, c.point = new b, c.point.prepare = function() {
                f.selection = !1;
                for (var a = 0; a < f._objects.length; a++) "point" == f._objects[a].type && (f._objects[a].hasBorders = !1, f._objects[a].lockMovementX = !0, f._objects[a].lockMovementY = !0, f._objects[a].selectable = !1);
                f.renderAll()
            }, c.point.begin = function(a) {
                var b;
                return (null == a.target || "point" !== a.target.type) && (b = new fabric.Circle({
                    left: a.e.offsetX,
                    top: a.e.offsetY,
                    fill: "#1e304c",
                    originX: "center",
                    originY: "center",
                    hasControls: !1,
                    hasBorders: !0,
                    radius: 6,
                    type: "point",
                    selectable: !1
                }), f.add(b)), b
            }, c.line = new b, c.line.queue = [], c.line.prepare = function() {
                f.selection = !1;
                for (var a = 0; a < f._objects.length; a++) "point" == f._objects[a].type && (f._objects[a].hasBorders = !1, f._objects[a].lockMovementX = !1, f._objects[a].lockMovementY = !1, f._objects[a].selectable = !1);
                f.renderAll()
            }, c.line.begin = function(a) {
                null !== a.target && "point" == a.target.type ? c.line.queue.push(a.target) : c.line.queue.push(c.point.begin(a))
            }, c.line.end = function(a) {
                if (2 == c.line.queue.length) {
                    var b = new fabric.Line([c.line.queue[c.line.queue.length - 2].left, c.line.queue[c.line.queue.length - 2].top, c.line.queue[c.line.queue.length - 1].left, c.line.queue[c.line.queue.length - 1].top], {
                        fill: "#1e304c",
                        stroke: "#1e304c",
                        strokeWidth: 1,
                        selectable: !1
                    });
                    f.add(b), c.line.queue = []
                }
            }, c.path = new b, c.path.prepare = function() {
                c.path.queue = [], c.line.prepare()
            }, c.path.begin = function(a) {
                null !== a.target && "point" == a.target.type ? c.path.queue.push(a.target) : c.path.queue.push(c.point.begin(a))
            }, c.path.end = function(a) {
                if (c.path.queue.length > 1) {
                    var b = new fabric.Line([c.path.queue[c.path.queue.length - 2].left, c.path.queue[c.path.queue.length - 2].top, c.path.queue[c.path.queue.length - 1].left, c.path.queue[c.path.queue.length - 1].top], {
                        fill: "#1e304c",
                        stroke: "#1e304c",
                        strokeWidth: 1,
                        selectable: !1
                    });
                    f.add(b)
                }
                3 === c.path.queue.length && (c.path.queue = [])
            }, c.$watch("toolbar.action", function() {
                c.toolbar.action.prepare()
            }), f.on("mouse:down", function(a) {
                c.toolbar.action.begin(a)
            }), f.on("mouse:move", function(a) {
                c.toolbar.action.process(a)
            }), f.on("mouse:up", function(a) {
                c.toolbar.action.end(a)
            }), c.undo = function() {
                c.elements.push(f._objects.pop()), f.renderAll()
            }, c.redo = function() {
                f._objects.push(c.elements.pop()), f.renderAll()
            }, c.reset = function() {
                f.clear(), f.loadFromJSON(c.originalState, f.renderAll.bind(f))
            }
        },
        templateUrl: "views/canvasComponentView.html"
    }
}), angular.module("ngApp").directive("bilDdd", function() {
    var a = function(a) {
        var b = document.createElement("div");
        b.className = "object", $(b).draggable({
            revert: "invalid"
        }), a.append(b)
    };
    return {
        transclude: !0,
        link: function(b, c, d) {
            var e = c.find("div.objectArea");
            a(e);
            var f = c.find("div.dropArea");
            f.droppable({
                drop: function(b, c) {
                    $(c.draggable.context).attr("style", "position:relative"), $(f).append(c.draggable.context), a(e)
                }
            })
        },
        templateUrl: "views/dddDirectiveView.html"
    }
}), angular.module("ngApp").directive("bilDDM", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilDds", function() {
    var a = function(a, b) {
            for (var c = [], d = 0; a > d; d++) {
                var e = Math.floor(Math.random() * (b - 0) + 0);
                ! function(a, b) {
                    c[a] = document.createElement("div"), c[a].className = "object type-" + b, c[a].draggable = !0
                }(d, e)
            }
            return c
        },
        b = function(a) {
            for (var b = [], c = 0; a > c; c++) {
                var d = document.createElement("div");
                d.className = "drop", b[c] = d
            }
            return b
        };
    return {
        transclude: !0,
        scope: {
            dropAreas: "@dropAreas",
            objects: "@objects"
        },
        link: function(c, d, e) {
            var f = d.find("div.objectList"),
                g = d.find("div.dropList");
            f.droppable({
                drop: function(a, b) {
                    $(b.draggable.context).attr("style", "position:relative"), $(f).append(b.draggable.context)
                }
            }), a(c.objects, c.dropAreas).forEach(function(a) {
                $(a).draggable({
                    revert: "invalid"
                }), f.append(a)
            }), b(c.dropAreas).forEach(function(a) {
                $(a).droppable({
                    drop: function(b, c) {
                        $(c.draggable.context).attr("style", "position:relative"), $(a).append(c.draggable.context)
                    }
                }), g.append(a)
            })
        },
        templateUrl: "views/ddsDirectiveView.html"
    }
}), angular.module("ngApp").directive("bilDt", function() {
    const a = 640,
        b = 640,
        c = "10",
        d = "black",
        e = "solid";
    var f = function(a) {
            this.canvas = a, this.state = {}, this.init = function(a) {}, this.begin = function(a) {}, this.process = function(a) {}, this.end = function(a) {}
        },
        g = function(a) {
            this.canvas = a;
            var b = {};
            for (var c in this) "canvas" !== c && (b[c] = this[c]());
            return b
        };
    g.prototype.mouse = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.isDrawingMode = !0, this.canvas.setAllItemsProperty("selectable", !0)
        }, a
    }, g.prototype.circle = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.isDrawingMode = !1, this.canvas.setAllItemsProperty("selectable", !1)
        }, a.begin = function(a) {
            null === a.target && (this.state.clicked = !0, this.state.element = new fabric.Circle({
                left: a.e.offsetX,
                top: a.e.offsetY,
                fill: this.canvas.freeDrawingBrush.color
            }), this.canvas.add(this.state.element))
        }, a.process = function(a) {
            if (this.state.clicked === !0 && null === a.target) {
                var b = this.state.element.top - a.e.offsetY;
                0 > b && (b = -1 * b);
                var c = b / 2;
                this.state.element.set({
                    radius: c
                }), this.state.element.setCoords(), this.canvas.renderAll()
            }
        }, a.end = function(a) {
            this.state.clicked = !1
        }, a
    }, g.prototype.square = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.isDrawingMode = !1, this.canvas.setAllItemsProperty("selectable", !1)
        }, a.begin = function(a) {
            null === a.target && (this.state.clicked = !0, this.state.element = new fabric.Rect({
                left: a.e.offsetX,
                top: a.e.offsetY,
                fill: this.canvas.freeDrawingBrush.color
            }), this.canvas.add(this.state.element))
        }, a.process = function(a) {
            if (this.state.clicked === !0 && null === a.target) {
                var b = this.state.element.left - a.e.offsetX;
                0 > b && (b = -1 * b);
                var c = this.state.element.top - a.e.offsetY;
                0 > c && (c = -1 * c), this.state.element.set({
                    width: b,
                    height: c
                }), this.state.element.setCoords(), this.canvas.renderAll()
            }
        }, a.end = function(a) {
            this.state.clicked = !1
        }, a
    }, g.prototype.triangle = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.isDrawingMode = !1, this.canvas.setAllItemsProperty("selectable", !1)
        }, a.begin = function(a) {
            null === a.target && (this.state.clicked = !0, this.state.element = new fabric.Triangle({
                left: a.e.offsetX,
                top: a.e.offsetY,
                fill: this.canvas.freeDrawingBrush.color
            }), this.canvas.add(this.state.element))
        }, a.process = function(a) {
            if (this.state.clicked === !0 && null === a.target) {
                var b = this.state.element.left - a.e.offsetX;
                0 > b && (b = -1 * b);
                var c = this.state.element.top - a.e.offsetY;
                0 > c && (c = -1 * c), this.state.element.set({
                    width: b,
                    height: c
                }), this.state.element.setCoords(), this.canvas.renderAll()
            }
        }, a.end = function(a) {
            this.state.clicked = !1
        }, a
    }, g.prototype.eraser = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.isDrawingMode = !0, this.canvas.freeDrawingBrush.color = "white", this.canvas.setAllItemsProperty("selectable", !1)
        }, a
    }, g.prototype.trash = function() {
        var a = new f(this.canvas);
        return a.init = function(a) {
            this.canvas.clear()
        }, a
    };
    var h = function(a) {
        for (var b in this)
            if (b === a) return this[b]();
        return []
    };
    return h.prototype.solid = function() {
        return [0, 0]
    }, h.prototype.dashed = function() {
        return [60, 40]
    }, h.prototype.dashdot = function() {
        return [30, 20, 0, 20]
    }, {
        transclude: !0,
        link: function(f, i, j) {
            fabric.Canvas.prototype.setAllItemsProperty = function(a, b) {
                for (var c = 0; c < this._objects.length; c++) this.item(c)[a] = b;
                this.renderAll()
            };
            var k = new fabric.Canvas(i.find("#canvas01").get(0));
            k.setWidth(a), k.setHeight(b), f.tools = new g(k), f.action = f.tools.mouse, f.color = d, f.width = c, f.line = e, f.isActiveTab = function(a) {
                return f.action === a
            }, f.$watch("action", function(a) {
                k.freeDrawingBrush.color = f.color, a.init()
            }), f.$watch("color", function(a) {
                k.freeDrawingBrush.color = a
            }), f.$watch("line", function(a) {
                k.freeDrawingBrush.strokeDashArray = new h(a)
            }), f.$watch("width", function(a) {
                k.freeDrawingBrush.width = parseInt(a)
            }), k.on("mouse:down", function(a) {
                f.action.begin(a)
            }), k.on("mouse:move", function(a) {
                f.action.process(a)
            }), k.on("mouse:up", function(a) {
                f.action.end(a)
            })
        },
        templateUrl: "views/dtDirectiveView.html"
    }
}), angular.module("ngApp").directive("bilEss", function() {
    return {
        transclude: !0,
        link: function(a, b, c) {
            b.find(".ckeditor").each(function(b, c) {
                CKEDITOR.replace(c, {
                    toolbar: [{
                        name: "basicstyles",
                        items: ["Bold", "Italic", "Underline", "BulletedList", "Subscript", "Superscript"]
                    }],
                    extraPlugins: "wordcount",
                    wordcount: {
                        showCharCount: !0,
                        maxCharCount: a.maxChars
                    }
                })
            })
        },
        templateUrl: "views/essDirectiveView.html",
        scope: {
            maxChars: "@maxChars"
        }
    }
}), angular.module("ngApp").directive("bilFC", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilFIB", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilIH", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilNl", function() {
    const a = 30,
        b = 6;
    return {
        transclude: !0,
        link: function(c, d, e) {
            c.pointerPosition = "up", e.min = parseInt(e.min), e.max = parseInt(e.max), e.scale = e.scale * a;
            var f = e.min < 0 ? -1 * e.min : e.min,
                g = (f + e.max) * e.scale + 12 + 1,
                h = new fabric.Canvas(d.find("#canvas").get(0));
            h.selection = !1, h.setWidth(g), h.setHeight(50), h.add(new fabric.Line([0, h.height / 2, h.width, h.height / 2], {
                stroke: "#000",
                strokeWidth: 2,
                selectable: !1,
                hasControls: !1,
                hasBorders: !1,
                lockMovementX: !1,
                lockMovementY: !1
            }));
            var i = 0,
                j = e.min;
            do {
                j = Math.round(e.min + i / e.scale), h.add(new fabric.Circle({
                    left: i + b,
                    top: h.height / 2,
                    fill: "#1e304c",
                    originX: "center",
                    originY: "center",
                    hasControls: !1,
                    hasBorders: !1,
                    radius: 3,
                    type: "point",
                    selectable: !1
                }));
                var k = new fabric.Text(j.toString(), {
                    left: i + b,
                    top: h.height / 2 + 5,
                    fontSize: 14,
                    fontWeight: "600",
                    selectable: !1,
                    fontFamily: "Source Sans Pro, sans-serif"
                });
                k.left -= k.width / 2, h.add(k), i += e.scale
            } while (i <= h.width && j < e.max);
            c.addPoint = function() {
                var a = new fabric.Triangle({
                    left: h.width / 2,
                    fill: "orange",
                    originX: "center",
                    originY: "center",
                    hasControls: !1,
                    hasBorders: !1,
                    type: "dragbable",
                    lockMovementX: !1,
                    lockMovementY: !0,
                    width: 15,
                    height: 10,
                    hoverCursor: "pointer"
                });
                "up" == c.pointerPosition ? (a.top = h.height / 2 - 8, a.setAngle(180)) : (a.top = h.height / 2 + 8, a.setAngle(0)), h.add(a), h.renderAll()
            }, c.upPoint = function() {
                c.pointerPosition = "up"
            }, c.downPoint = function() {
                c.pointerPosition = "down"
            };
            var l = [];
            h.on("object:added", function(a) {
                for (var b = 0; b < h._objects.length; b++) "dragbable" === h._objects[b].type && void 0 === h._objects[b].hasLine && (h._objects[b].hasLine = !0, l.push(h._objects[b]));
                if (2 == l.length) {
                    var c = l[0],
                        d = l[1],
                        e = new fabric.Line([c.left, h.height / 2, d.left, h.height / 2], {
                            stroke: "orange",
                            strokeWidth: 2,
                            selectable: !1,
                            hasControls: !1,
                            hasBorders: !1,
                            lockMovementX: !1,
                            lockMovementY: !1,
                            type: "line"
                        });
                    c.line = {
                        target: e,
                        index: "x1"
                    }, d.line = {
                        target: e,
                        index: "x2"
                    }, l = [], h.add(e)
                }
            }), h.on("object:moving", function(a) {
                void 0 !== a.target.line && ("x2" == a.target.line.index && a.target.line.target.set({
                    x2: a.target.left
                }), "x1" == a.target.line.index && a.target.line.target.set({
                    x1: a.target.left
                }), a.target.line.target.setCoords()), a.target.left > h.width && a.target.set({
                    left: h.width - b - 3
                }), a.target.left < b && a.target.set({
                    left: b
                })
            }), c.isActiveTab = function(a) {
                return a === c.pointerPosition
            }
        },
        templateUrl: "views/nlDirectiveView.html"
    }
}), angular.module("ngApp").directive("bilPNC", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilSDT", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilST", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").directive("bilImage", function() {
    function a(a, b, c) {}
    return {
        transclude: !0,
        link: a
    }
}), angular.module("ngApp").run(["$templateCache", function(a) {
    a.put("views/audioComponentView.html", '<div class="autoplay"> <!--<span class="title">Autoplay</span>\r\n    <button class="btn btn-initial" ng-click="audioToggle()">\r\n        <svg width="68" height="38" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="audio == true">\r\n            <use xlink:href="../../static/images/Buttons/autoplay-toggle-on.svg#Page-1" />\r\n        </svg>\r\n        <svg width="68" height="38" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="audio == false">\r\n            <use xlink:href="../../static/images/Buttons/autoplay-toggle-off.svg#Page-1" />\r\n        </svg>\r\n    </button>\r\n    --> <span>Autoplay</span> <div class="toggle-group"> <div class="toggle" ng-class="{enable: audio === true}" ng-click="audioToggle([\'off\',\'on\'])"> <div class="switcher">{{ audioText }}</div> <span class="text">off</span> <span class="text">on</span> </div> <div class="dlm"></div> <div class="toggle" ng-class="{enable: lang === true}" ng-click="langToggle([\'en\',\'es\'])"> <div class="switcher">{{ langText }}</div> <span class="text">en</span> <span class="text">es</span> </div> </div> </div>'), a.put("views/canvasComponentView.html", '<div class="cp-container"> <div class="toolbar"> <button ng-class="{active:isActiveTab(mouse)}" ng-click="onClickTab(mouse)"> <i class="fa fa-mouse-pointer" aria-hidden="true"></i> </button> <button ng-class="{active:isActiveTab(point)}" ng-click="onClickTab(point)"> <i class="fa fa-circle" aria-hidden="true"></i>&nbsp;&nbsp;Point </button> <button ng-class="{active:isActiveTab(line)}" ng-click="onClickTab(line)">Line</button> <button ng-class="{active:isActiveTab(path)}" ng-click="onClickTab(path)">Path</button> <button ng-click="undo()"> <i class="fa fa-undo" aria-hidden="true"></i>&nbsp;&nbsp;Undo </button> <button ng-click="redo()"> <i class="fa fa-repeat" aria-hidden="true"></i>&nbsp;&nbsp;Redo </button> <button ng-click="reset()"> <i class="fa fa-times" aria-hidden="true"></i>&nbsp;&nbsp;Reset </button> </div> <canvas id="cp"></canvas> </div>'), a.put("views/captionsComponentView.html", '<div class="captions"> <div class="container"> <div class="row"> <div class="col-xs-12"> <button class="btn btn-initial toggle" ng-click="captionsToggle()"> <svg width="32" height="26" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="captions == true"> <use xlink:href="../../static/images/Buttons/caption-toggle-on.svg#Page-1"> </svg> <svg width="32" height="26" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="captions == false"> <use xlink:href="../../static/images/Buttons/caption-toggle-off.svg#Page-1"> </svg> </button> <span ng-show="captions == true"> <!-- caption text --> </span> </div> </div> </div> </div>'), a.put("views/dddDirectiveView.html", '<div class="ddd-directive"> <div class="objectArea"></div> <div class="dropArea"></div> </div>'), a.put("views/ddsDirectiveView.html", '<div class="dds-directive"> <div class="objectList"></div> <div class="dropList"></div> </div>'), a.put("views/dtDirectiveView.html", '<div class="cnv-container"> <div class="toolbar" ng-init="action = tools.mouse"> <button ng-click="action = tools.mouse" ng-class="{active:isActiveTab(tools.mouse)}"><i class="fa fa-mouse-pointer" aria-hidden="true"></i> Mouse</button> <button ng-click="action = tools.circle" ng-class="{active:isActiveTab(tools.circle)}"><i class="fa fa-circle-o" aria-hidden="true"></i> Circle</button> <button ng-click="action = tools.square" ng-class="{active:isActiveTab(tools.square)}"><i class="fa fa-square" aria-hidden="true"></i> Square</button> <button ng-click="action = tools.triangle" ng-class="{active:isActiveTab(tools.triangle)}"><i class="fa fa-caret-down" aria-hidden="true"></i> Triangle</button> <button ng-click="action = tools.eraser" ng-class="{active:isActiveTab(tools.eraser)}"><i class="fa fa-eraser" aria-hidden="true"></i> Eraser</button> <button ng-click="action = tools.trash"><i class="fa fa-trash" aria-hidden="true"></i> Trash</button> <span class="dropdown"> <span class="title">width</span> <select ng-model="width"> <option value="10">1</option> <option value="30">3</option> <option value="50">5</option> </select> </span> <span class="dropdown"> <span class="title">color</span> <select ng-model="color"> <option value="red">red</option> <option value="black">black</option> <option value="green">green</option> </select> </span> <span class="dropdown"> <span class="title">line</span> <select ng-model="line"> <option value="solid">solid</option> <option value="dashed">dashed</option> <option value="dashdot">dashdot</option> </select> </span> </div> <canvas id="canvas01"></canvas> </div>'), a.put("views/essDirectiveView.html", '<textarea class="ckeditor"></textarea>'), a.put("views/exm02/6-1.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-xs-12 content"><img src="../../static/images/Picture1.svg" alt="Alt text"></div> <div class="col-xs-2"> <audio-component src="en=./audio/cat1.mp3;es=./audio/cat1_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/exm02/6-2.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-sm-12 header"> <b>Write the prime factorization of 48.</b><br> </div> <div class="col-sm-12 content"> <img src="../../static/images/tree-first.svg" alt="Alt text"> </div> <div class="col-xs-2"> <audio-component src="en=./audio/cat2.mp3;es=./audio/cat2_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/exm02/6-3.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-sm-12 header"> <b>Write the prime factorization of 48.</b><br> Choose any factor pair of 48 to begin the factor tree </div> <div class="col-sm-12 content"> <img src="../../static/images/tree-first-with-expression.svg" alt="Alt text"> </div> <div class="col-xs-2"> <audio-component src="en=./audio/cat3.mp3;es=./audio/cat3_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/exm02/6-4.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-sm-12 header"> <b>Write the prime factorization of 48.</b><br> Choose any factor pair of 48 to begin the factor tree </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-first-with-popup.svg" alt="Alt text"> </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-second.svg" alt="Alt text"> </div> <div class="col-xs-2"> <audio-component src="en=./audio/cat4.mp3;es=./audio/cat4_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/exm02/6-5.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-sm-12 header"> <b>Write the prime factorization of 48.</b><br> Choose any factor pair of 48 to begin the factor tree </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-first-with-popup.svg" alt="Alt text"> </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-second-with-expression.svg" alt="Alt text"> </div> <div class="col-xs-2"> <audio-component src="en=./audio/cat5.mp3;es=./audio/cat5_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/exm02/6-6.html", '<div class="exm01 theme-1"> <div class="container slide"> <div class="row"> <div class="col-sm-12 header"> <b>Write the prime factorization of 48.</b><br> Choose any factor pair of 48 to begin the factor tree </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-first-with-popup.svg" alt="Alt text"> </div> <div class="col-sm-6 content"> <img src="../../static/images/tree-second-with-popup.svg" alt="Alt text"> <popup-component left="225px" top="225px"> Notice that beginning with<br> different factor pairs results in the<br> same prime factorization. Every<br> composite number has only one<br> prime factorization. </popup-component> </div> <div class="col-xs-2"> <audio-component src="en=./audio/cat6.mp3;es=./audio/cat6_es.mp3;"></audio-component> </div> <div class="col-xs-10"> <navigation-component controller-as="main"></navigation-component> </div> </div> </div> <captions-component></captions-component> </div>'), a.put("views/navigationComponentView.html", '<div class="col-xs-12 navigation"> <!-- play/stop button --> <button ng-click="play()" class="btn" ng-show="visibility.play == true"> <svg width="44" height="44" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="switches.play == true"> <use xlink:href="../../static/images/Buttons/audio-toggle-pause.svg#Page-1"> </svg> <svg width="44" height="44" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" ng-show="switches.play == false"> <use xlink:href="../../static/images/Buttons/audio-toggle-play.svg#Page-1"> </svg> </button> <!-- prev button --> <a ng-click="prev()" class="btn" ng-show="visibility.prev == true"> <svg width="100" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <use xlink:href="../../static/images/Buttons/back.svg#Page-1"> </svg> </a> <!-- next button --> <a ng-click="next()" class="btn" ng-show="visibility.next == true"> <svg width="100" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <use xlink:href="../../static/images/Buttons/next.svg#Page-1"> </svg> </a> <!-- show all --> <a ng-click="showAll()" class="btn btn-initial right" ng-show="visibility.showAll == true"> <svg width="48" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="-7 -7 48 45"> <use xlink:href="../../static/images/Buttons/show-all.svg#Page-1"> </svg> </a> <!-- reset btn --> <a ng-click="reset()" class="btn btn-initial right" ng-show="visibility.reset == true"> <svg width="48" height="45" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewbox="-10 -10 48 45"> <use xlink:href="../../static/images/Buttons/reload.svg#Page-1"> </svg> </a> </div>'), a.put("views/nlDirectiveView.html", '<div class="cnv-container"> <div class="toolbar"> <button ng-click="addPoint()"><i class="fa fa-plus" aria-hidden="true"></i></button> <button ng-click="upPoint()" ng-class="{active:isActiveTab(\'up\')}"><i class="fa fa-caret-up" aria-hidden="true"></i></button> <button ng-click="downPoint()" ng-class="{active:isActiveTab(\'down\')}"><i class="fa fa-caret-down" aria-hidden="true"></i></button> </div> <canvas id="canvas"></canvas> </div>'), a.put("views/popupComponentView.html", '<div class="more-info" style="left: {{left}}; top: {{top}}"> <a class="more-info-btn" ng-click="toggle()"> <svg width="39" height="44" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <use xlink:href="../../static/images/Buttons/more-info-button.svg#Page-1"> </svg> </a> <div class="popup" ng-show="show === true" ng-transclude></div> </div>'), a.put("views/svgComponentView.html", '<div ng-bind-html="content"></div>'), a.put("views/test.html", "<bil-cp></bil-cp>"), a.put("views/tst.html", "<bil-dt></bil-dt>")
}]);