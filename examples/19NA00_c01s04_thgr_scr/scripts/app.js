'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'screens/1_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/1_02', {
            templateUrl: 'screens/1_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/1_03', {
            templateUrl: 'screens/1_03.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2_01', {
            templateUrl: 'screens/2_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2_02', {
            templateUrl: 'screens/2_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2_03', {
            templateUrl: 'screens/2_03.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2_04', {
            templateUrl: 'screens/2_04.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/3_01', {
            templateUrl: 'screens/3_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/3_02', {
            templateUrl: 'screens/3_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4_01', {
            templateUrl: 'screens/4_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4_02', {
            templateUrl: 'screens/4_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4_03', {
            templateUrl: 'screens/4_03.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4_04', {
            templateUrl: 'screens/4_04.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/5_01', {
            templateUrl: 'screens/5_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/6_01', {
            templateUrl: 'screens/6_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/6_02', {
            templateUrl: 'screens/6_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/7_01', {
            templateUrl: 'screens/7_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/7_02', {
            templateUrl: 'screens/7_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8_01', {
            templateUrl: 'screens/8_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8_02', {
            templateUrl: 'screens/8_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8_03', {
            templateUrl: 'screens/8_03.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8_04', {
            templateUrl: 'screens/8_04.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8_05', {
            templateUrl: 'screens/8_05.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9_01', {
            templateUrl: 'screens/9_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9_02', {
            templateUrl: 'screens/9_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10_01', {
            templateUrl: 'screens/10_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10_02', {
            templateUrl: 'screens/10_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10_03', {
            templateUrl: 'screens/10_03.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10_04', {
            templateUrl: 'screens/10_04.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10_05', {
            templateUrl: 'screens/10_05.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/11_01', {
            templateUrl: 'screens/11_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/12_01', {
            templateUrl: 'screens/12_01.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/12_02', {
            templateUrl: 'screens/12_02.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/tst', {
            templateUrl: 'views/tst.html',
            controllerAs: 'main2'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
