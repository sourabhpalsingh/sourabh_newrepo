'use strict';
/**
 * @ngdoc overview
 * @name bilApp
 * @description
 * # ngApp
 * 
 * v0.0.1   
 *
 * Main module of the application.
 */
angular.module('ngApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
]).config(["$routeProvider", function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'screens/1.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/1a', {
            templateUrl: 'screens/1a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/2', {
            templateUrl: 'screens/2.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2a', {
            templateUrl: 'screens/2a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/2b', {
            templateUrl: 'screens/2b.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/3', {
            templateUrl: 'screens/3.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/3a', {
            templateUrl: 'screens/3a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/4', {
            templateUrl: 'screens/4.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4a', {
            templateUrl: 'screens/4a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4b', {
            templateUrl: 'screens/4b.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/4c', {
            templateUrl: 'screens/4c.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/5', {
            templateUrl: 'screens/5.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/5a', {
            templateUrl: 'screens/5a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/6', {
            templateUrl: 'screens/6.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/6a', {
            templateUrl: 'screens/6a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/6b', {
            templateUrl: 'screens/6b.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/6c', {
            templateUrl: 'screens/6c.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/7', {
            templateUrl: 'screens/7.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8', {
            templateUrl: 'screens/8.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/8a', {
            templateUrl: 'screens/8a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9', {
            templateUrl: 'screens/9.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9a', {
            templateUrl: 'screens/9a.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/9b', {
            templateUrl: 'screens/9b.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
		.when('/10', {
            templateUrl: 'screens/10.html',
            controller: 'ExmController',
            controllerAs: 'main'
        })
        .when('/tst', {
            templateUrl: 'views/tst.html',
            controllerAs: 'main2'
        })
        .otherwise({
            redirectTo: '/'
        });
}]);
