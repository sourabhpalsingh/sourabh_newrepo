'use strict';

/**
 * @ngdoc function
 * @name ngApp.controller:ExmController
 * @description
 * # ExmController
 * Controller of the ngApp
 */

angular.module('ngApp').controller('ExmController', ['$scope', '$location', 'routeProvider', 'toggleProvider', function ($scope, $location, routeProvider, toggleProvider) {

    $scope.end = false;

    routeProvider.init('main');

    $scope.$on('navigationComponent:play', function (event, data) {
        toggleProvider.set('audioCompoment:audio:play', data.toggle);
    });

    $scope.$on('audioCompoment:audio:ended', function (event, data) {
        $scope.$apply(function () {
            routeProvider.next();
            if (routeProvider.isEnd()) {
                toggleProvider.set('navigationComponent:play', false);
                $scope.end  = true;
            }
        });
    });

    $scope.$on('navigationComponent:next', function (event, data) {
        routeProvider.next();
    });

    $scope.$on('navigationComponent:prev', function (event, data) {
        routeProvider.prev();
    });

    $scope.$on('navigationComponent:showAll', function (event, data) {
        routeProvider.last();
    });

    $scope.$on('navigationComponent:reset', function (event, data) {
        routeProvider.first();
    });

    $scope.$on('navigationComponent:init', function () {
        if (routeProvider.isEnd()) {
            toggleProvider.set('navigationComponent:visibility:showAll', false);
            toggleProvider.set('navigationComponent:visibility:reset', true);
            toggleProvider.set('navigationComponent:visibility:next', false);
            toggleProvider.set('navigationComponent:visibility:prev', true);
        }
        if (routeProvider.isFirst()) {
            toggleProvider.set('navigationComponent:visibility:play', true);
            toggleProvider.set('navigationComponent:visibility:reset', false);
            toggleProvider.set('navigationComponent:visibility:showAll', true);
            toggleProvider.set('navigationComponent:visibility:prev', false);
            toggleProvider.set('navigationComponent:visibility:next', true);
        }
        if(!routeProvider.isFirst() && !routeProvider.isEnd()) {
            toggleProvider.set('navigationComponent:visibility:reset', true);
            toggleProvider.set('navigationComponent:visibility:showAll', true);
            toggleProvider.set('navigationComponent:visibility:prev', true);
            toggleProvider.set('navigationComponent:visibility:next', true);
        }
    });
}]);
